flask
uwsgi
psycopg2-binary
# The following are not needed for deployment, just testing.  If they are
# part of the test image as well as the development and test environment,
# it is not necessary for them to be part of the deployed image.
#flask-pytest
#pytest-cov
