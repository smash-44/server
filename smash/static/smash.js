var timer = 0;
var default_refresh = 300;
var default_switch_refresh = 5;
var refresh_interval;
var switch_interval;
var last_refreshed;

// cookie stuff stolen from https://www.w3schools.com/js/js_cookies.asp

function setCookie(key, value, exdays=365) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = key + "=" + value + ";" + expires + ";path=/";
}

function getCookie(key) {
  var name = key + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie(key, default_value) {
  var value = getCookie(key);
  if (value != "") {
    return value;
  } else {
    setCookie(key, default_value);
    return default_value;
  }
}

function onLoad() {

  refresh = checkCookie('refresh', default_refresh);
  switch_refresh = checkCookie('switch_refresh', default_switch_refresh);

  refresh_interval = refresh * 1000;
  switch_interval = switch_refresh * 1000;

  var now = new Date();
  last_refreshed = now.toLocaleString();

  if (!document.hidden) {
    timer = setInterval(reload, refresh_interval);
  }
  if (document.addEventListener) {
    document.addEventListener("visibilitychange", visibilityChanged);
  }

  document.getElementById('refresh').value = refresh;

  document.getElementById('last_refreshed').innerHTML = last_refreshed;
}

function visibilityChanged() {
  clearTimeout(timer);
  if (!document.hidden) {
    timer = setInterval(switch_reload, switch_interval);
  }
}

function switch_reload() {
  clearTimeout(timer);
  timer = setInterval(reload, refresh_interval);
  location.reload(true);
}

function reload() {
  location.reload(true);
}

function selectRefresh() {
  var refresh = document.getElementById('refresh').value;
  refresh_interval = refresh * 1000;

  clearTimeout(timer);
  timer = setInterval(reload, refresh_interval);

  setCookie('refresh', refresh);
}
