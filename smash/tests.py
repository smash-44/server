# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import mdal

# ---------------------------------------------------------------------------
#                                                               SQL queries
# ---------------------------------------------------------------------------


# ---------------------------------------------------------------------------
#                                                           factory methods
# ---------------------------------------------------------------------------


# ---------------------------------------------------------------------------
#                                                                Test class
# ---------------------------------------------------------------------------

class Test(mdal.Persistent):

  table = 'tests'
  key = 'name'
  persistence = {
    'name': {
      'type': 'string'
    },
    'description': {
      'type': 'string'
    }
  }

  def __init__(self, name=None, description=None, new=False):

    if name and not new:
      # lookup
      super().__init__(name)
    else:
      # new object
      super().__init__()
      self.name = name
      self.description = description
