# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import time
from datetime import datetime
from flask import Blueprint, render_template

from .nodes import get_nodes
from . import results


bp = Blueprint('dashboard', __name__, url_prefix='/')


def humanize_delta(seconds):

  if abs(seconds) < 90:
    return "%ds" % seconds

  if abs(seconds) < 90 * 60:
    return "%dm" % (seconds / 60)

  if abs(seconds) < 48 * 3600:
    return "%dh" % (seconds / 3600)

  if abs(seconds) < 14 * 86400:
    return "%dd" % (seconds / 86400)

  if abs(seconds) < 3 * 604800:
    return "%dw" % (seconds / 604800)

  if abs(seconds) < 12 * 2592000:
    return "%dmo" % (seconds / 2592000)

  return "%dy" % (seconds / 31536000)


# ---------------------------------------------------------------------------
#                                                                    routes
# ---------------------------------------------------------------------------

@bp.route('/', methods=['GET'])
def dashboard():

  # organize results by node
  nodes = get_nodes()

  # get latest results of all tests
  latest = results.get_latest()

  for result in latest or []:
    nodes[result.node].latest.append(result)

    # humanize some of the data
    # determine age
    try:
      delta = datetime.utcfromtimestamp(time.time()) - result.recorded
    except TypeError as e:
      # TODO: This will happen if sqlite3.PARSE_DECLTYPES not declared as part
      # of new db connection.  Will probably need a more robust solution in
      # MDAL that works for Postgres.
      print("TypeError: {}".format(e))
      assert False
    delta_secs = delta.total_seconds()
    result.age = humanize_delta(delta_secs)

    # determine staleness
    # TODO: get from site, node and test configuration
    result.stale = (delta_secs > 2 * 60 * 60)

  # render
  return render_template('dashboard.html', nodes=nodes)
