# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import mdal

# ---------------------------------------------------------------------------
#                                                             consts, sorta
# ---------------------------------------------------------------------------

status_map = {
  0: 'Okay',
  1: 'Unknown',
  2: 'Warning',
  3: 'Error',
  4: 'Unusable'
}

# ---------------------------------------------------------------------------
#                                                               SQL queries
# ---------------------------------------------------------------------------

RESULTS_GET_LATEST = {
  'sqlite': """
    SELECT    id, node, test, status, message, MAX(recorded) AS "recorded [timestamp]"
    FROM      results
    GROUP BY  node, test
    """,
  'postgres': """
    SELECT    DISTINCT ON (node, test)
              id, node, test, status, message, recorded
    FROM      results
    ORDER BY  node, test, recorded DESC
    """
}


# ---------------------------------------------------------------------------
#                                                           factory methods
# ---------------------------------------------------------------------------

def get_latest():
  db = mdal.get_db()

  qres = db.execute(RESULTS_GET_LATEST[db.type]).fetchall()

  # create list of results
  if qres:
    return [
      Result(record=rec) for rec in qres
    ]
  return None

# ---------------------------------------------------------------------------
#                                                              Result class
# ---------------------------------------------------------------------------

class Result(mdal.Persistent):

  table = 'results'
  persistence = {
    'id': {
      'type': 'integer'
    },
    'node': {
      'type': 'string'
    },
    'test': {
      'type': 'string'
    },
    'status': {
      'type': 'integer'
    },
    'message': {
      'type': 'string'
    },
    'recorded': {
      'type': 'timestamp'
    }
  }

  def __init__(self, id=None, node=None, test=None, status=None, message=None, record=None):

    if record:
      # factory load
      super().__init__(record=record)
    elif id:
      # lookup
      super().__init__(id)
    else:
      # new object
      super().__init__()
      self.node = node
      self.test = test
      self.status = status
      self.message = message

  @property
  def status_text(self):
    if self.status is None:
      raise ValueError("Attempt to report on undefined status")
    return status_map[self.status]
