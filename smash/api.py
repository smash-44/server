# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from flask import Blueprint, request, abort, make_response
import mdal
from . import nodes
from . import tests
from . import results

bp = Blueprint('api', __name__, url_prefix='/api')

# ---------------------------------------------------------------------------
#                                                            error handlers
# ---------------------------------------------------------------------------

@bp.errorhandler(500)
def server_error(error):
  # TODO: error description does not show up in log
  print("ERROR (500): %s" % (error.description,))
  return make_response("ERROR: %s" % (error.description,), 500)


@bp.errorhandler(400)
def request_error(error):
  # TODO: error description does not show up in log
  print("ERROR (400): %s" % (error.description,))
  return make_response("ERROR: %s" % (error.description,), 400)

# ---------------------------------------------------------------------------
#                                                                    routes
# ---------------------------------------------------------------------------

@bp.route('/node', methods=['POST'])
def register_node():

  try:
    nodekey = request.form['key']
    nodename = request.form['name']
  except KeyError:
    abort(400, 'Node key and name must be supplied')

  try:
    nodes.Node(name=nodename)
  except mdal.exceptions.ObjectNotFound:
    pass
  else:
    abort(400, "A node with the name '%s' already exists" % (nodename,))

  try:
    node = nodes.Node(name=nodename, key=nodekey)
    node.commit()
  except Exception as e:
    abort(500, 'Unable to update database: %s' % (e,))

  return "OK", 201


@bp.route('/result', methods=['POST'])
def record_result():

  # required request fields
  try:
    nodename = request.form['node']
    nodekey = request.form['key']
    test = request.form['test']
    status = request.form['status']
  except KeyError:
    abort(400, 'Node key and name, test, and status must all be specified')

  # message is optional
  message = request.form.get('message', None)

  # validate node
  try:
    n = nodes.Node(nodename)
    assert n.key == nodekey
  except (mdal.exceptions.ObjectNotFound, AssertionError):
    abort(403)

  # validate test
  try:
    t = tests.Test(name=test)
  except mdal.exceptions.ObjectNotFound:
    # create test object
    t = tests.Test(name=test, new=True)
    t.commit()

  # record result in database
  try:
    r = results.Result(status=status, message=message, node=n.name, test=t.name)
    r.commit()
  except Exception as e:
    abort(500, 'Unable to update database: %s' % (e,))

  return "OK", 201
