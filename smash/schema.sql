DROP TABLE IF EXISTS site;
DROP TABLE IF EXISTS nodes;
DROP TABLE IF EXISTS tests;
DROP TABLE IF EXISTS results;

CREATE TABLE nodes (
  name TEXT UNIQUE NOT NULL,
  ip TEXT, -- leaving out UNIQUE and NOT NULL at the moment as may be behind gateway,
  key TEXT NOT NULL,
  registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE tests (
  name TEXT UNIQUE NOT NULL,
  description TEXT
);

-- status: 0 = okay, 1 = warning, 2 = errors, 3 = unusable
CREATE TABLE results (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  node TEXT NOT NULL,
  test TEXT NOT NULL,
  status INTEGER NOT NULL,
  message TEXT,
  recorded TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (node) REFERENCES nodes(name),
  FOREIGN KEY (test) REFERENCES tests(name)
);

-- for site data, for example:
-- * server public/private keys
-- * schema version in use
-- * etc.
CREATE TABLE site (
  key TEXT PRIMARY KEY,
  value TEXT NOT NULL
);

