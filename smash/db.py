# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import click
from flask.cli import with_appcontext
from flask import current_app
import mdal


def init_db():
  db = mdal.get_db()

  if db.type == 'sqlite':
    schema = "schema.sql"
  elif db.type == 'postgres':
    schema = "schema.psql"
  else:
    # TODO: proper exception
    raise Exception(
      "Did not catch proper DB connection type.  Module, class: {}".format(
        type(db).__module + '.' + type(db).__qualname__
      )
    )

  with current_app.open_resource(schema) as f:
    db.executescript(f.read().decode('utf8'))
  db.commit()


def seed_db(seedfile):
  db = mdal.get_db()

  with current_app.open_resource(seedfile) as f:
    db.executescript(f.read().decode('utf8'))
  db.commit()


@click.command('init-db')
@with_appcontext
def init_db_command():
  """Clear the existing data and create new tables."""
  init_db()
  click.echo('Initialized the database.')
