# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=C0415
#
import os
from flask import Flask
from flask.cli import with_appcontext
import mdal


env_prefix = 'SMASH_'


def create_app(test_config=None):

  # create app object
  app = Flask(__name__, instance_relative_config=True)

  # default database URI
  default_database_uri = "file:///%s/%s.sqlite" % (
    app.instance_path, __name__)

  # create app configuration
  app.config.from_mapping(
    SECRET_KEY='dev',
    DATABASE_URI=os.environ.get(env_prefix + 'DATABASE_URI',
                                default_database_uri),
  )

  if test_config is None:
    # load the instance config, if it exists, when not testing
    app.config.from_pyfile('config.py', silent=True)
  else:
    # load the test config if passed in
    app.config.from_mapping(test_config)

  # ensure the instance folder exists (only relevant for SQLite database)
  if app.config['DATABASE_URI'].startswith('file:'):
    try:
      os.makedirs(app.instance_path)
    except OSError:
      # TODO: do something!
      pass

  # TODO: logging would be super
  #from . import log
  #app.teardown_appcontext(log.close_log)

  from . import db
  # TODO: is not needed?
  #app.teardown_appcontext(db.close_db)

  # register CLI commands
  app.cli.add_command(db.init_db_command)
  #app.cli.add_command(db.seed_db_command)
  #app.cli.add_command(db.upgrade_db_command)

  # register API
  from . import api
  app.register_blueprint(api.bp)

  # register dashboard
  from . import dashboard
  app.register_blueprint(dashboard.bp)

  # configure database access
  mdal.configure(app.config['DATABASE_URI'])

  return app
