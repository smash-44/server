# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import mdal

# ---------------------------------------------------------------------------
#                                                               SQL queries
# ---------------------------------------------------------------------------

GET_ALL_NODES = """
  SELECT    name, key, ip, registered
  FROM      nodes
"""

# ---------------------------------------------------------------------------
#                                                           factory methods
# ---------------------------------------------------------------------------

def get_nodes():
  db = mdal.get_db()
  res = db.execute(GET_ALL_NODES).fetchall()
  if res:
    return {
      row['name']: Node(record=row)
      for row in res
    }
  return None

# ---------------------------------------------------------------------------
#                                                                Node class
# ---------------------------------------------------------------------------

class Node(mdal.Persistent):

  table = 'nodes'
  key = 'name'
  persistence = {
    'name': {
      'type': 'string'
    },
    'key': {
      'type': 'string'
    },
    'ip': {
      'type': 'string'
    },
    'registered': {
      'type': 'timestamp'
    }
  }

  def __init__(self, name=None, key=None, ip=None, registered=None, record=None):

    # transient, used for results
    self._latest = None

    if record:
      # factory load
      super().__init__(record=record)
    elif not key:
      # lookup
      super().__init__(name)
    else:
      # new object
      super().__init__()
      self.name = name
      self.key = key
      self.ip = ip
      # TODO: `None` should ~= NULL
      if registered:
        self.registered = registered

  @property
  def latest(self):
    if not self._latest:
      self._latest = []
    return self._latest
