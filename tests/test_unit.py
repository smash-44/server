# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=bad-whitespace
#
import re
from smash import create_app

# ---------------------------------------------------------------------------
#                                                                    BASICS
# ---------------------------------------------------------------------------

def test_config():
  assert not create_app().testing
  assert create_app({'TESTING': True}).testing

def test_root(client):
  response = client.get('/')
  assert response.status_code == 200

# ---------------------------------------------------------------------------
#                                                           EMPTY DASHBOARD
# Really, need to make fixtures have class scope, and group tests in batches
# that make sense, otherwise will miss lots of tests--like this one.
# ---------------------------------------------------------------------------

def test_dashboard_no_results(client):
  """
  GIVEN a Flask application,
  WHEN the dashboard is accessed after the above tests,
  THEN the dashboard shows two nodes and their status.
  """
  response = client.get('/')
  assert response.status_code == 200


# ---------------------------------------------------------------------------
#                                                         NODE REGISTRATION
# ---------------------------------------------------------------------------

def test_node_registration(client):
  """
  GIVEN a Flask application,
  WHEN a new node registers with a node name and key
  THEN the registration is successful.
  """
  response = client.post('/api/node', data=dict(
    name = 'testnode1',
    key  = 'testnode1key'
  ))
  assert response.status_code == 201

def test_node_registration_duplicate_name(client):
  """
  GIVEN a Flask application,
  WHEN a new node registers with a node name that already exists
  THEN the registration fails.
  """
  response = client.post('/api/node', data=dict(
    name = 'testnode1',
    key  = 'testnode1keyotherthing'
  ))
  assert response.status_code == 400

def test_node_registration_duplicate_key(client):
  """
  GIVEN a Flask application,
  WHEN a new node registers with a key that already exists
  THEN the registration succeeds.

  This test case exists to remind myself that this is actually valid.
  """
  response = client.post('/api/node', data=dict(
    name = 'testnode2',
    key  = 'testnode1key'
  ))
  assert response.status_code == 201

def test_node_registration_missing_name(client):
  """
  GIVEN a Flask application,
  WHEN a new node registers with a key but no name
  THEN the registration fails.
  """
  response = client.post('/api/node', data=dict(
    key  = 'testnode3key'
  ))
  assert response.status_code == 400

# ---------------------------------------------------------------------------
#                                                            NODE REPORTING
# ---------------------------------------------------------------------------

def test_record_results(client):
  """
  GIVEN a Flask application,
  WHEN a node reports test results with no message
  THEN the report is recorded.
  """
  response = client.post('/api/result', data=dict(
    node   = 'testnode1',
    key    = 'testnode1key',
    test   = 'test1',
    status = 0
  ))
  assert response.status_code == 201

def test_record_subsequent_results(client):
  """
  GIVEN a Flask application,
  WHEN a node reports test results for new run of test with message
  THEN the report is recorded.
  """
  response = client.post('/api/result', data=dict(
    node    = 'testnode1',
    key     = 'testnode1key',
    test    = 'test1',
    status  = 1,
    message = 'Now with message'
  ))
  assert response.status_code == 201

def test_record_results_bad_request(client):
  """
  GIVEN a Flask application,
  WHEN a node reports test results without required fields
  THEN the report is rejected.
  """
  response = client.post('/api/result', data=dict(
    node   = 'testnode1',
    key    = 'testnode1key',
    test   = 'test1'
  ))
  assert response.status_code == 400

def test_record_results_bad_creds(client):
  """
  GIVEN a Flask application,
  WHEN a node reports test results with a bad key
  THEN the report is rejected.
  """
  response = client.post('/api/result', data=dict(
    node   = 'testnode1',
    key    = 'testnode1keyWRONG',
    test   = 'test1',
    status = 0
  ))
  assert response.status_code == 403

def test_record_results_second_node(client):
  """
  GIVEN a Flask application,
  WHEN a second node reports test results
  THEN the report is recorded.
  """
  response = client.post('/api/result', data=dict(
    node   = 'testnode2',
    key    = 'testnode1key',
    test   = 'test1',
    status = 1
  ))
  assert response.status_code == 201

# ---------------------------------------------------------------------------
#                                                                 DASHBOARD
# ---------------------------------------------------------------------------

def test_basic_dashboard(client):
  """
  GIVEN a Flask application,
  WHEN the dashboard is accessed after the above tests,
  THEN the dashboard shows two nodes and their status.
  """
  response = client.get('/')
  assert response.status_code == 200
  assert b'<caption>testnode1</caption>' in response.data and b'<caption>testnode2</caption>' in response.data

def test_result_ordering(client):
  """
  GIVEN a Flask application,
  WHEN the dashboard is accessed with seeded test results,
  THEN the latest results are presented per test, per node
  NOTE it would be cleaner if this was tested via an API
  """
  response = client.get('/')
  assert response.status_code == 200

  pstr = r"""<caption>fakenode1</caption>.*
        <tr class='Okay'>
          <th>faketest1</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Okay</td>
          <td>recent</td>
        </tr>.*
        <tr class='Unknown_stale'>
          <th>faketest2</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unknown<br/><mark>STALE</mark></td>
          <td>recent</td>
        </tr>.*
        <tr class='Unknown'>
          <th>faketest3</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unknown</td>
          <td>recent</td>
        </tr>.*
        <tr class='Warning'>
          <th>faketest4</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Warning</td>
          <td>recent</td>
        </tr>.*
        <caption>fakenode2</caption>.*
        <tr class='Unusable'>
          <th>faketest1</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unusable</td>
          <td>recent</td>
        </tr>.*
        <tr class='Error'>
          <th>faketest2</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Error</td>
          <td>recent</td>
        </tr>.*
        <tr class='Unusable'>
          <th>faketest3</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unusable</td>
          <td>recent</td>
        </tr>.*
        <tr class='Okay'>
          <th>faketest4</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Okay</td>
          <td>recent</td>
        </tr>.*
        <caption>fakenode3</caption>.*
        <tr class='Error'>
          <th>faketest1</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Error</td>
          <td>recent</td>
        </tr>.*
        <tr class='Unusable'>
          <th>faketest2</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unusable</td>
          <td>recent</td>
        </tr>.*
        <tr class='Okay'>
          <th>faketest3</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Okay</td>
          <td>recent</td>
        </tr>.*
        <tr class='Error'>
          <th>faketest4</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Error</td>
          <td>recent</td>
        </tr>.*
        <caption>fakenode4</caption>.*
        <tr class='Error'>
          <th>faketest1</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Error</td>
          <td>recent</td>
        </tr>.*
        <tr class='Unknown'>
          <th>faketest2</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unknown</td>
          <td>recent</td>
        </tr>.*
        <tr class='Warning'>
          <th>faketest3</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Warning</td>
          <td>recent</td>
        </tr>.*
        <tr class='Unknown'>
          <th>faketest4</th>
          <td><span title='[^']*'>(\d+[smhdwoy])</span></td>
          <td>Unknown</td>
          <td>recent</td>
        </tr>.*"""
  p = re.compile(pstr, re.DOTALL)

  # check results against basic pattern
  m = p.search(response.data.decode('utf-8'))
  if not m:
    print(response.data.decode('utf-8'))
  assert m is not None

  # check that all values are within reasonable distance of expected
  expected = ['40m', '9h', '46s', '11m',
              '38s', '10s', '60m', '15s',
              '18s', '47s', '8s', '1s',
              '25s', '44s', '11s', '6s']
  secsfactor = {
    's': 1,
    'm': 60,
    'h': 3600,
    'd': 86400,
    'w': 604800,
    'mo': 2592000,
    'y': 31536000
  }
  pstr = r'(\d+)([shmdoy])'
  p = re.compile(pstr)
  def hrtos(hr):
    m = p.match(hr)
    if m:
      return int(m[1]) * secsfactor[m[2]]
    return 0

  # this test is sensitive to delay between seeding of database and test run
  # and what passes easily locally might not do as well on my less performant
  # CI runners.  The fake results that are only "seconds" old are senstive to
  # this.  This threshold hopefully will need no further adjustment.
  observed_delay_threshold = 10

  # compare times on results
  acceptable = 0
  for idx, expected_seconds in enumerate(map(hrtos, expected)):
    observed_seconds = hrtos(m.group(idx+1))
    if abs(observed_seconds - expected_seconds) < observed_delay_threshold:
      acceptable += 1
    #else:
    #  # useful in failures to adjust delay threshold
    #  print("Index %d: %s, observed - expected = %d - %d = %d" % (
    #    idx, expected[idx], observed_seconds, expected_seconds,
    #    observed_seconds - expected_seconds
    #  ))
  assert acceptable == len(expected)
