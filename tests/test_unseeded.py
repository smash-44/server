# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=bad-whitespace
#


# ---------------------------------------------------------------------------
#                                                COMPLETELY EMPTY DASHBOARD
# ---------------------------------------------------------------------------

def test_dashboard_no_results_at_all(empty_client):
  """
  GIVEN the Smash app,
  WHEN  the database is freshly initialized and there are no registered
        nodes or test results,
  THEN  the dashboard shows empty but does not throw an error.
  """
  response = empty_client.get('/')
  assert response.status_code == 200
