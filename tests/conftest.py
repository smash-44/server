# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import os
import tempfile

import pytest
import mdal
from smash import create_app
from smash.db import init_db, seed_db


# load test data
# might just use data created by other tests
#with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
#  _data_sql = f.read().decode('utf8')


# database parameters
(sqlite_fh, sqlite_fn) = tempfile.mkstemp()
sqlite_params = {
  'schema': 'schema.sql',
  'seed': 'seed.sql',
  'uri': 'file://' + sqlite_fn,
  'delete_afterwards': True,
  'filehandle': sqlite_fh,
  'filename': sqlite_fn
}
pgsql_params = {
  'schema': 'schema.psql',
  'seed': 'seed.psql',
  'uri': 'postgresql://postgres:supersecretpassword@localhost:5432/postgres'
}


#@pytest.fixture(scope='module', params=[sqlite_params], ids=['sqlite'])
#@pytest.fixture(scope='module', params=[pgsql_params], ids=['pgsql'])
@pytest.fixture(scope='module', params=[sqlite_params, pgsql_params], ids=['sqlite', 'pgsql'])
# pylint: disable=W0621
def app(request):

  app = create_app({
    'TESTING': True,
    'DATABASE_URI': request.param['uri']
  })

  with app.app_context():
    mdal.configure(app.config['DATABASE_URI'])
    init_db()
    seed_db(os.path.join(os.path.dirname(__file__), request.param['seed']))

  yield app

  mdal.close()
  if request.param.get('delete_afterwards') and request.param.get('filename'):
    os.unlink(request.param['filename'])

  return app

# pylint: disable=W0621
@pytest.fixture(scope='module')
def client(app):
  return app.test_client()


# These are for tests requiring empty databases.  There's gotta be a better
# way of doing this
@pytest.fixture(scope='module', params=[sqlite_params, pgsql_params], ids=['sqlite', 'pgsql'])
def empty_app(request):

  app = create_app({
    'TESTING': True,
    'DATABASE_URI': request.param['uri']
  })

  with app.app_context():
    mdal.configure(app.config['DATABASE_URI'])
    init_db()

  yield app

  mdal.close()
  if request.param.get('delete_afterwards') and request.param.get('filename'):
    os.unlink(request.param['filename'])

  return app

@pytest.fixture(scope='module')
def empty_client(empty_app):
  return empty_app.test_client()
