# based on https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04

from smash import create_app

app = create_app()

if __name__ == "__main__":
  app = create_app()
  app.run(host='0.0.0.0', port=8000)
